<?php

require_once '../includes/db_handler.php';
require '.././libs/Slim/Slim.php';

\Slim\Slim::registerAutoloader();

$app = new \Slim\Slim();

$user_id = null;

/**
 * Adding Middle Layer to authenticate every request
 * Checking if the request has valid api key in the 'Authorization' header
 */
function authenticate(\Slim\Route $route)
{
    // Getting request headers
    $headers  = apache_request_headers();
    $response = array();
    $app      = \Slim\Slim::getInstance();

    // Verifying Authorization Header
    if (isset($headers['Authorization'])) {
        $db = new DbHandler();

        // get the api key
        $api_key = $headers['Authorization'];
        // validating api key
        if (!$db->isValidApiKey($api_key)) {
            // api key is not present in users table
            $response["error"]   = true;
            $response["message"] = "Access Denied. Invalid Api key";
            echoResponse(401, $response);
            $app->stop();
        } else {
            global $user_id;
            // get user primary key id
            $user_id = $db->getUserId($api_key);
        }
    } else {
        // api key is missing in header
        $response["error"]   = true;
        $response["message"] = "Api key is misssing";
        echoResponse(400, $response);
        $app->stop();
    }
}
/**
 * Register users
 * method POST
 * url /users/
 */
$app->post('/user_login/', function () use($app){
            $username = $app->request()->post('username');
            $password = $app->request()->post('password');
            $response = array();


            $db = new DbHandler();
 
            if ($db->checkLogin($username, $password)) {
                // get the user by username
                $user = $db->getUserByUsername($username);
 
                if ($user != NULL) {
                    $response["error"] = false;
                    $response['user_id'] = $user['user_id'];
                    $response['user_username'] = $user['user_username'];
                    $response['user_email'] = $user['user_email'];
                    $response['message'] = "Successful Login";
                } else {
                    // unknown error occurred
                    $response['error'] = true;
                    $response['message'] = "An error occurred. Please try again";
                }
            } else {
                // user credentials are wrong
                $response['error'] = true;
                $response['message'] = 'Login failed. Incorrect credentials';
            }
    echoResponse(200, $response);
});
/**
 * Register users
 * method POST
 * url /users/
 */
$app->post('/users/', function () use($app){
        $response = array();
            $user = $app->request->post('user');
 
            global $user_id;
            $db = new DbHandler();
 
            // creating new task
            //$task_id = $db->createTask($user_id, $task);
 
            if ($task_id != NULL) {
                $response["error"] = false;
                $response["message"] = "New user created successfully";
                $response["task_id"] = $task_id;
            } else {
                $response["error"] = true;
                $response["message"] = "Failed to create new user. Please try again";
            }
    echoResponse(200, $response);
});

/**
 * Listing single user
 * method GET
 * url /users/:id
 */
$app->get('/users/:id', function ($user_id) {
    $response = array();
    $db       = new DbHandler();

    $result = $db->getUserDetails($user_id);

    $response["error"]        = count($result) > 0;
    $response["user_id"]      = $user_id;
    $response["user_details"] = array();
    if ($result != null) {
        while ($user_details = $result->fetch_assoc()) {
            $tmp                        = array();
            $tmp["user_username"]       = $user_details["user_username"];
            $tmp["user_password"]       = $user_details["user_password"];
            $tmp["user_display_name"]   = $user_details["user_display_name"];
            $tmp["user_email"]          = $user_details["user_email"];
            $tmp["user_profile_image"]  = $user_details["user_profile_image"];
            $tmp["user_type_login"]     = $user_details["user_type_login"];
            $tmp["user_facebook"]       = $user_details["user_facebook"];
            $tmp["user_google"]         = $user_details["user_google"];
            $tmp["user_contact_number"] = $user_details["user_contact_number"];
            array_push($response["user_details"], $tmp);
        }
        echoResponse(200, $response);
    } else {
        $response["error"]   = true;
        $response["message"] = "The requested resource doesn't exists";
        echoResponse(404, $response);
    }
});

/**
 * Echoing json response to client
 * @param String $status_code Http response code
 * @param Int $response Json response
 */
function echoResponse($status_code, $response)
{
    $app = \Slim\Slim::getInstance();
    // Http response code
    $app->status($status_code);

    // setting response content type to json
    $app->contentType('application/json');

    echo json_encode($response);
}

$app->run();
