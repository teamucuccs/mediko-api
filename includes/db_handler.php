<?php

/**
 * Class to handle all db operations
 * This class will have CRUD methods for database tables
 *
 */
class DbHandler {

    private $conn;

    function __construct() {
        require_once dirname(__FILE__) . '/db_connect.php';
        // opening db connection
        $db = new DbConnect();
        $this->conn = $db->connect();
    }



    /**
     * Fetching all menus of a restaurant
     * 
     */
    public function getUserDetails($user_id) {
        $stmt = $this->conn->prepare("SELECT * from tbl_users WHERE user_id = ?");
        $stmt->bind_param('i', $user_id);
        if ($stmt->execute()) {
            $users = $stmt->get_result();
            $stmt->close();
            return $users;
        } else {
            return NULL;
        }
    }
 public function checkLogin($username, $password) {
        // fetching user by email
        $stmt = $this->conn->prepare("SELECT user_password FROM tbl_users WHERE user_username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $stmt->bind_result($password_hash);
        $stmt->store_result();
 
        if ($stmt->num_rows > 0) {
            // Found user with the email
            // Now verify the password
 
            $stmt->fetch();
            $stmt->close();
            /*if (PassHash::check_password($password_hash, $password)) {*/
            if ($password_hash == $password) {
                return TRUE;
            } else {
                return FALSE;
            }
        } else {
            $stmt->close();
            return FALSE;
        }
    }


    /**
     * Checking for duplicate user by email address
     * @param String $email email to check in db
     * @return boolean
     */
    private function isUserExists($user_username) {
        $stmt = $this->conn->prepare("SELECT user_id from tbl_users WHERE user_username = ?");
        $stmt->bind_param("s", $username);
        $stmt->execute();
        $stmt->store_result();
        $num_rows = $stmt->num_rows;
        $stmt->close();
        return $num_rows > 0;
    }
 
    /**
     * Fetching user by username
     * @param String $username Usermame
     */
    public function getUserByUsername($username) {
        $stmt = $this->conn->prepare("SELECT * FROM tbl_users WHERE user_username = ?");
        $stmt->bind_param("s", $username);
        if ($stmt->execute()) {
            $user = $stmt->get_result()->fetch_assoc();
            $stmt->close();
            return $user;
        } else {
            return NULL;
        }
    }

   /**
     * 
     * 
     */
    public function registerUser($user_id) {
        $stmt = $this->conn->prepare("SELECT * from tbl_users WHERE user_id = ?");
        $stmt->bind_param('i', $user_id);
        if ($stmt->execute()) {
            $users = $stmt->get_result();
            $stmt->close();
            return $users;
        } else {
            return NULL;
        }
    }




}

?>
